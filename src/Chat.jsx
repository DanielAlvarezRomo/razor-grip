import React, { useState } from 'react';
import 'firebase/auth';
import Userx from './useUser';
import { useFirebaseApp } from 'reactfire';
import { useUser } from 'reactfire';

const Chat =(props) => {
  const user = useUser();
  const [error, setError] = useState(null);
  const [isLoaded, setIsLoaded] = useState(false);
  const [items, setItems] = useState([]);
  const [message, setMessage] = useState('');
  const [emailChat, setEmailChat] = useState('');
  const [room, setRoom] = useState('');
  const [to, setContact] = useState('');
  const [allMesages, setAllMessages] = useState('');
  const io2 = require('socket.io-client');
  let socket2 = io2('https://razor-grip.herokuapp.com', { auth: { token: user.data.Aa } });
  const firebase = useFirebaseApp();

  const logout = async ()=>{
    await firebase.auth().signOut();
    window.location.reload();
  }

    const blockUser = (jwt, data) => {
      block('https://razor-grip.herokuapp.com/api/users/block', { "users": [ data ] },  jwt)
      .then(data => {
        console.log(data);
      });
      fetch("https://razor-grip.herokuapp.com/api/users/",{
        headers: {
            'Authorization': user.data.Aa
        }
    })
      .then(res => res.json())
      .then(
        (result) => {
          setIsLoaded(true);
          console.log(result);
          setItems(result.data);
        },
  
        (error) => {
          setIsLoaded(true);
          setError(error);
        }
      )
  }

/*
  const getOnline = async ()=>{
  socket2.emit('getdata');
  socket2.on('getdata', function(data){
    console.log(data);
  })
}*/




  setTimeout(() => {
socket2.on(room.room_id, function(data){
      console.log(data);
    })
  }, 500);

  async function block(url = '', data = {}, jwt) {

  const response = await fetch(url, {
    method: 'PATCH', 
    cache: 'no-cache',
    headers: {
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': 'no-cors',
      'Authorization': jwt
    },
    body: JSON.stringify(data), 
      referrerPolicy: 'no-referrer'
  });
  return response.json(); 
}

  async function send() {
    setTimeout(() => {
      socket2.emit('new-message', { room_id: room.room_id, to: to, message: message });
      console.log('socket');
      console.log(socket2.id);
      createRoom(to,user.data.Aa,emailChat);

  }, 500)
    setMessage("");
    socket2.on(room.room_id, function(data){
      createRoom(to,user.data.Aa,emailChat);
    })

    fetch("https://razor-grip.herokuapp.com/api/users/",{
      headers: {
          'Authorization': user.data.Aa
      }
  })
    .then(res => res.json())
    .then(
      (result) => {
        setIsLoaded(true);
        console.log(result);
        setItems(result.data);
      },

      (error) => {
        setIsLoaded(true);
        setError(error);
      }
    )
 
  }

  const handleUserInput = (ev) => {
      setMessage(ev.target.value);
  };
  
  const createRoom = (uid, jwt, email) => {
      createroom('https://razor-grip.herokuapp.com/api/chats/rooms/'+uid,  jwt)
      .then(data => {
        console.log(data);
        setRoom(data);
        setContact(uid); // JSON data parsed by `data.json()` call
      fetch("https://razor-grip.herokuapp.com/api/chats/messages/"+data.room_id,{
        headers: {
            'Authorization': jwt
        }
    })
      .then(response => response.json())
      .then(mensajes => {
        setAllMessages(mensajes.data);
        console.log(mensajes.data);
      }
        ); 
      });

      fetch("https://razor-grip.herokuapp.com/api/users/",{
        headers: {
            'Authorization': user.data.Aa
        }
    })
      .then(res => res.json())
      .then(
        (result) => {
          setIsLoaded(true);
          console.log(result);
          setItems(result.data);
        },
  
        (error) => {
          setIsLoaded(true);
          setError(error);
        }
      )

      setEmailChat(email);
      
  }

  async function createroom(url = '', jwt) {

  const response = await fetch(url, {
    method: 'POST', // *GET, POST, PUT, DELETE, etc.
    cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
    headers: {
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': 'no-cors',
      'Authorization': jwt
    },
    referrerPolicy: 'no-referrer' // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
  });
  return response.json(); // parses JSON response into native JavaScript objects
}

React.useEffect(() => {
  fetch("https://razor-grip.herokuapp.com/api/users/",{
      headers: {
          'Authorization': user.data.Aa
      }
  })
    .then(res => res.json())
    .then(
      (result) => {
        setIsLoaded(true);
        console.log(result);
        setItems(result.data);
      },

      (error) => {
        setIsLoaded(true);
        setError(error);
      }
    )
}, [])

if (error) {
  return <div>Error: {error.message}</div>;
} else if (!isLoaded) {
  return <div>Loading...</div>;
} else {
  return (
    <div id="Chats">
      <nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#">Chat</a>
  <button class="btn btn-dark" onClick={logout}>Log out</button>  
  <div class="collapse navbar-collapse" id="navbarNav">
  </div>
</nav>
<Userx></Userx>
    <div class="card">
Online Contacts
  <ul class="list-group list-group-flush">
  {items.map(item => (
            <li class="list-group-item" key={item.id}>
                {item.status === 'ON' && 
                <div>
                <button class="btn btn-light" onClick={() => createRoom(item.uid,user.data.Aa,item.email)}>{item.email}</button>
                <button class="btn btn-danger" onClick={() => blockUser(user.data.Aa, item.uid)}>block</button>
                </div>
                }
                {!items &&
                  <div>No Users Connected</div>
                }
            </li>
            ))}
  </ul>
</div>


<div class="card" id="Box">
  <div class="card-body">
    { allMesages &&
            <div id ='allMessages'>                
              {allMesages.map(mensaje => (
              <div key={mensaje.message_id}>
                  {mensaje.from == user.data.uid && 
                    <div id="propios">You</div>
                  }
                  {mensaje.from != user.data.uid && 
                    <div id="recibidos">{emailChat}</div>
                  }
                    {mensaje.message}
                  
              </div>
              ))}</div>
    }
  </div>
</div>
        
            <div  id="inputMessages">
            {room && 
            <div>
                <div id="chatBox">
                <input type="text" id='CHATBOX' value={message} placeholder="Enter Message" onChange={handleUserInput}/>
                <button id="sendMessage" onClick={send}>Send</button>
                </div>
            </div>}
            </div>
        
    </div>
  );
}
}



export default Chat;