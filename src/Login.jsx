import React, {useState} from 'react';
import 'firebase/auth';
import { useFirebaseApp } from 'reactfire';

async function fetchMethods(url = '', data = {}, jwt, method) {

    const response = await fetch(url, {
  method: method, // *GET, POST, PUT, DELETE, etc.
  cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
  headers: {
    'Content-Type': 'application/json',
    'Authorization': jwt
  },
  referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
  body: JSON.stringify(data) // body data type must match "Content-Type" header
});
return response.json(); // parses JSON response into native JavaScript objects
}

const addUsr = (jwt, data) => {
fetchMethods('https://razor-grip.herokuapp.com/api/users/add', { email: data  }, jwt, 'POST')
.then(data => {
console.log(data); // JSON data parsed by `data.json()` call
});
}

const Login =(props) => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const firebase = useFirebaseApp();
    const submit = async()=>{
        try{
        await firebase.auth().createUserWithEmailAndPassword(email,password);
        
            firebase.auth().onAuthStateChanged(function(user) {
                if (user) {
                user.getIdToken().then(function(idToken) {
                    addUsr(idToken,email)
                                
                });
                }
            })
    } catch(error){
        alert(error.message)
    }
    }

    const login = async ()=>{
        try{
        await firebase.auth().signInWithEmailAndPassword(email,password);
            firebase.auth().onAuthStateChanged(function(user) {
                if (user) {
                user.getIdToken().then(function(idToken) {
                    console.log(idToken);          
                });
                }
            })
        } catch(error){
            alert(error.message)
        }
    }

    return(
        
            <div id="center-item">
                <div id="FormLogin">
                <input htmlFor="email" id="email" placeholder="Enter email" onChange={ (ev) => setEmail(ev.target.value)}/>
                <input type="password" id="password" placeholder="Enter password" autoComplete="on" onChange={ (ev) => setPassword(ev.target.value)}/>
                <button onClick={login}>Login</button>
                <button onClick={submit}>Register</button>
                </div>
            </div>
        
    )
};

export default Login;