import React, { useState } from 'react';
import { useUser } from 'reactfire';

function Users() {
    const user = useUser();
    const [error, setError] = useState(null);
    const [isLoaded, setIsLoaded] = useState(false);
    const [items, setItems] = useState([]);
    const [message, setMessage] = useState('');
    const [received, serReceived] = useState('');
    const [room, setRoom] = useState('');
    const [to, setContact] = useState('');
    const io2 = require('socket.io-client');
    let socket2 = io2('https://razor-grip.herokuapp.com', { auth: { token: user.data.Aa } });

    const blockUser = (jwt, data) => {
        block('https://razor-grip.herokuapp.com/api/users/block', { "users": [ data ] },  jwt)
        .then(data => {
          console.log(data);
        });
    }

    async function block(url = '', data = {}, jwt) {

    const response = await fetch(url, {
      method: 'PATCH', 
      cache: 'no-cache',
      headers: {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': 'no-cors',
        'Authorization': jwt
      },
      body: JSON.stringify(data), 
        referrerPolicy: 'no-referrer'
    });
    return response.json(); 
}


    async function send() {
      setTimeout(() => {
        socket2.emit('new-message', { room_id: room.room_id, to: to, message: message });
        console.log('socket');
        console.log(socket2.id);
    }, 500)
      setMessage("");
      socket2.on(room.room_id, function(data){
        console.log(data)
        serReceived(data.message);
      })

    }

    const handleUserInput = (ev) => {
        setMessage(ev.target.value);
    };
    
    
    const createRoom = (uid, jwt) => {
        createroom('https://razor-grip.herokuapp.com/api/chats/rooms/'+uid,  jwt)
        .then(data => {
          console.log(data.room_id);
          setRoom(data);
          setContact(uid); // JSON data parsed by `data.json()` call
        });
        getMessages('https://razor-grip.herokuapp.com/api/chats/messages/'+data.room_id, jwt)
    }

    async function createroom(url = '', jwt) {

    const response = await fetch(url, {
      method: 'POST', // *GET, POST, PUT, DELETE, etc.
      cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
      headers: {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': 'no-cors',
        'Authorization': jwt
      },
      referrerPolicy: 'no-referrer' // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
    });
    return response.json(); // parses JSON response into native JavaScript objects
}

async function getMessages(url = '', jwt) {

    const response = await fetch(url, {
      headers: {
        'Authorization': jwt
      },
    });
    return response.json(); // parses JSON response into native JavaScript objects
}
  

    React.useEffect(() => {
      fetch("https://razor-grip.herokuapp.com/api/users/",{
          headers: {
              'Authorization': user.data.Aa
          }
      })
        .then(res => res.json())
        .then(
          (result) => {
            setIsLoaded(true);
            console.log(result);
            setItems(result.data);
          },

          (error) => {
            setIsLoaded(true);
            setError(error);
          }
        )
    }, [])

  
    if (error) {
      return <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
      return <div>Loading...</div>;
    } else {
      return (
        <div>

            <div id="Contactos">
                {items.map(item => (
                <div key={item.id}>
                    {item.status === 'ON' && 
                    <div>
                    <button id="contacs" onClick={() => createRoom(item.uid,user.data.Aa)}>{item.email}</button>
                    <button onClick={() => blockUser(user.data.Aa, item.uid)}>block</button>
                    </div>
                    } 
                </div>
                ))}
            </div>
            
            <div id="MessagePane">
                <div id="MessagesBox">
                { received &&
                <div>{received}</div>
                }
                </div>
                <div id="inputMessages">
                {room && 
                <div>
                    <div id="chatBox">
                    <input type="text" id='CHATBOX' value={message} placeholder="Enter Message" onChange={handleUserInput}/>
                    <button id="sendMessage" onClick={send}>Send</button>
                    </div>
                </div>}
                </div>
            </div>
        </div>
      );
    }
  }
export default Users;