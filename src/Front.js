import './App.css';
import './App.scss';
import Login from './Login';
import { useUser } from 'reactfire';
import Chat from './Chat';

function Front() {
  const user = useUser();

  return (
    <div className="App">
      {
                !user.data && <Login></Login>

        }
            {
                user.data &&  
                <div>
                  <Chat></Chat> 
                </div> 
        }
    </div>
  );
}

export default Front;
