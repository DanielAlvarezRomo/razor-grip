import React, { useState } from 'react';
import { useUser } from 'reactfire';
import 'firebase/auth';
import Chat from './Chat';

const Chatbox =(props) => {
    const user = useUser();
    const [message, setMessage] = useState('');

    const io2 = require('socket.io-client');
    const socket = io2('https://middleware-razor.herokuapp.com:3000', { auth: { token: user.data.Aa } });
    console.log('listening socket from Chatbox ' + user.data.Aa);
   
    const send = () => {
      setTimeout(() => {
        socket.emit('new-message', { room_id: room.room_id, to: 'K1b1bWxu3AUV8hy9xuYTtwpQo123', message: message });
        console.log('room ' + room.room_id + ' mensaje: ' + message);
    }, 500)
    }
    
    

    return(
        <div id="chatBox">
            <Chat></Chat>
            <input type="text" placeholder="Enter Message" onChange={ (ev) => setMessage(ev.target.value)}/>
            <button onClick={send}>Enviar</button>
        </div>
    )
};

export default Chatbox;